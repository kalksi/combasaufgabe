package Repo;

import java.util.HashMap;

import com.Kalksi.clientDemoBasicAuth.models.Beleg;
import com.Kalksi.clientDemoBasicAuth.models.Results;

public class CashManager {
	
	
	private static HashMap<String, String> hmap;
	
	private static boolean isThere = false;

	
	public static void cashIdWithNumber(Beleg belege) {
		
		
		hmap = new HashMap<String, String>();
		
		for (Results result : belege.getResults()) {
			
			hmap.put(result.getNumber(), result.getId());
			
		}
		
	}
	
	
	
	public static String getIdFromNumber(String number) {
		
		if(hmap.containsKey(number)) {
			isThere= true;
			return 	hmap.get(number);
			
		}
		
		isThere= false;
		return 	"die Belegnummer existiert nicht!";
		
		
		
	}
	
	public static boolean  youCanSearch() {
		return isThere;
		
		
	
	}
	

}
