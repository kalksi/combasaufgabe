package com.Kalksi.clientDemoBasicAuth;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.Base64;


import com.Kalksi.clientDemoBasicAuth.models.Beleg;
import com.Kalksi.clientDemoBasicAuth.models.Results;
import com.fasterxml.jackson.databind.ObjectMapper;






public class Abfragen {
	
	private HttpClient client;
	private String encoding ; 
	
	public Abfragen() {
		client = HttpClient.newHttpClient();
		 encoding = Base64.getEncoder().encodeToString(("kha@combase.de:(13topsecret37]").getBytes());
	}
	
	public void getAll(String uri) throws IOException, InterruptedException {

		HttpRequest request = HttpRequest.newBuilder()
				.uri(URI.create(uri))
				.setHeader("Authorization", "Basic " + encoding)
				.build();
			System.out.println(request.headers());
		HttpResponse<String> response = null;
	
			response = client.send(request, BodyHandlers.ofString());
			
			//System.out.println(response.body());
			
			Beleg belege = new ObjectMapper().readValue(response.body(), Beleg.class);
		//result.forEach(System.out::println);
			
		/*	for(Results r :belege.getResults()) {
				System.out.println(r.getItems());
			}
			*/
			
			belege.getResults().stream().map(r -> r.getItems()).forEachOrdered(System.out::println);
			
			
	}
	

}
