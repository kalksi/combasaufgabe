package com.Kalksi.clientDemoBasicAuth.Views;

import java.util.ArrayList;
import java.util.List;

import com.Kalksi.clientDemoBasicAuth.models.Beleg;
import com.Kalksi.clientDemoBasicAuth.models.Items;
import com.Kalksi.clientDemoBasicAuth.models.Payments;
import com.Kalksi.clientDemoBasicAuth.models.Results;
import com.Kalksi.clientDemoBasicAuth.viewModels.Artikelpositionen;
import com.Kalksi.clientDemoBasicAuth.viewModels.DetailsViewContainer;
import com.Kalksi.clientDemoBasicAuth.viewModels.SimpleViewContainer;
import com.Kalksi.clientDemoBasicAuth.viewModels.Zahlungsmittel;

public class OutputFormatter {

	private static List<SimpleViewContainer> simpleView = new ArrayList<SimpleViewContainer>();

	static String Output(Beleg beleg) {

		int i = 1;
		for (Results result : beleg.getResults()) {
			SimpleViewContainer oneResult = new SimpleViewContainer(result.getNumber(),
					result.getPointOfSale().getName(), result.getOrganizationalUnit().getName(),
					result.getBookingTime(),
					(float) result.getPayments().stream().mapToDouble(d -> d.getAmount()).sum());

			System.out.println(i++ + "-" + oneResult);
			simpleView.add(oneResult);
		}

		return null;

	}

	static String OutputWithDetails(Beleg belege) {
		
		int i = 0;
		
		for (SimpleViewContainer sv : simpleView) {
			
			List<Artikelpositionen> artikelpositionenList = new ArrayList<Artikelpositionen> ();
			List<Zahlungsmittel> zahlungsmittelList = new ArrayList<Zahlungsmittel> ();
			
			
            for( Items it : belege.getResults().get(i).getItems()) {
            	
            	artikelpositionenList.add(new Artikelpositionen(
            			it.getProduct().getName(),
            			it.getQuantity(), 
            			it.getTotal().getBase()));
            };
            
            for( Payments P : belege.getResults().get(i).getPayments()) {
            	
            	zahlungsmittelList.add(new Zahlungsmittel(P.getPaymentMethod().getName(), P.getAmount()) );
            	
            }
            	
			DetailsViewContainer dv = new DetailsViewContainer(sv, belege.getResults().get(i).getCashier().getName(),
					artikelpositionenList, zahlungsmittelList);
			
			System.out.println(i + "-" + dv);
			
			
			i++;

		}

		return null;

	}

}
