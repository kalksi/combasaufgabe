package com.Kalksi.clientDemoBasicAuth.Views;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

import com.Kalksi.clientDemoBasicAuth.DependencyManagement;
import com.Kalksi.clientDemoBasicAuth.models.Beleg;
import com.Kalksi.clientDemoBasicAuth.services.Interface_BelegeService;

import Repo.CashManager;

public class UserConsole {

	private Interface_BelegeService belegeService;
	private Beleg belege;

	private String BaseUrl = "https://www.koronacloud.com/web/api/v3/accounts/";
	private String UserID = "16329894-40cd-47de-b93f-84064504edf7/";
	private Scanner scanner;
	String[] Optionen = { "Alle Belege Abfragen", "einen Beleg anhand eine Belegnummer suchen", "Alle Belege aus Kasse",
			"einen Belege aus Kasse anhand eine Belegnummer suchen", "update cash" };

	private int testvar;

	public UserConsole() {
		belegeService = DependencyManagement.getBelegeService();
		UpdateChach();
		scanner = new Scanner(System.in);

	}

	public void run() throws InterruptedException {

		menu();

		while (true) {

			try {
				Thread.sleep(900);
				System.err.println("was wollen Sie machen?");

				Thread.sleep(900);
				System.out.print(" für Menu typen Sie menu ein...... oder! für schlissen typen Sie exit ein.\n");
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			var eingabe = scanner.nextLine();
			if (eingabe.equals("exit")) {
				System.err.println("byyyyyyyyyy!!");
				break;
			} else {

				switch (eingabe) {
				case "1":

					OutputFormatter.Output(belege);
					System.out.println("wollen Sie mehr Details? y/n ");
					if (getInput().contentEquals("y")) {
						OutputFormatter.OutputWithDetails(belege);

					}
					Thread.sleep(900);
					break;
				case "2":
					System.err.println("Bitte Belegnummer eingeben:\n");

					var tmp = CashManager.getIdFromNumber(getInput());
					System.out.println(tmp);
					if (CashManager.youCanSearch()) {
						System.out.println(belegeService.getByID(BaseUrl + UserID + "receipts", tmp));

					}
					System.err.println("\n");
					break;

				case "3":
					System.err.println("Bitte Kassenummer eingeben:\n");
					if (Integer.parseInt(getInput()) == 1) {
						System.out.println(belegeService.getAllByPointOfSale(BaseUrl + UserID + "pointsOfSale/",
								"947b5a1b-dd1a-4b5d-9857-354e91b73841"));
						break;
					}
					System.err.println("Falsche Eingabe!:\n");
					break;
				case "4":
					System.err.println("Bitte Kassenummer eingeben:\n");
					String kasse = getInput();
					System.err.println("Bitte Belegnummer eingeben:\n");
					String nummer = getInput();
					String id = CashManager.getIdFromNumber(nummer);
					// kassen Bereich ist nur zwischen 1 und 1
					if (CashManager.youCanSearch() && Integer.parseInt(kasse) == 1) {
						System.out.println("ok....\n");
						System.out.println(belegeService.getOneByPointOfSale(BaseUrl + UserID + "pointsOfSale/",
								"947b5a1b-dd1a-4b5d-9857-354e91b73841", id));
						break;
					}

					System.err.println("Falsche Eingabe!:\n");
					break;
				case "5":
					UpdateChach();

					break;

				case "menu":
					menu();
					break;
				default:
					menu();
					break;
				}

				Thread.sleep(900);

			}

		}

	}

	public void UpdateChach() {
		System.out.println("Cashing......");
		belege = belegeService.getAll(BaseUrl + UserID + "receipts");
		CashManager.cashIdWithNumber(belege);
	}

	Runnable printItOut = () -> System.out.println(
			"Ich habe hier keine sinnvolle Funktion! ich diene nur zum testen(Lambda als Parameter in Asyncfunktion überzugeben)"
					+ ++testvar);

	/*
	 * public void UpdateUserConsol(List<Model> list) {
	 * 
	 * list.forEach(System.out::println); //testvar hat keine Funktion! testvar = 5;
	 * System.out.println(testvar); }
	 * 
	 * private void filter(String suchwort) throws Exception {
	 * 
	 * List<Model> posts = postsService.getAll(BaseUrl + "posts"); int ergebnisse =
	 * 0; for (Model model : posts) { if (model.getTitle().contains(suchwort)) {
	 * System.out.println(model); ergebnisse++; } }
	 * 
	 * if (ergebnisse == 0) { System.out.println("leider keine Ergibnesse\n");
	 * 
	 * } else { System.out.println(ergebnisse + " Ergibnesse wurde gefunden"); }
	 * 
	 * }
	 * 
	 * private List<Model> sortByTitle(List<Model> list) {
	 * 
	 * Collections.sort(list, new Comparator<Model>() { { }
	 * 
	 * @Override public int compare(Model o1, Model o2) { // TODO Auto-generated
	 * method stub return o1.getTitle().compareTo(o2.getTitle()); } });
	 * 
	 * return list;
	 * 
	 * }
	 */
	private String getInput() {

		return scanner.nextLine();
	}

	private void menu() {

		System.out.println("da sind Ihre Optionen;\n");

		for (int i = 0; i < Optionen.length; i++) {

			System.out.println(i + 1 + "- " + Optionen[i] + "\n");
		}
	}
}
