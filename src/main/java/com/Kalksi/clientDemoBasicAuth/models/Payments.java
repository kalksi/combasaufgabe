package com.Kalksi.clientDemoBasicAuth.models;

public class Payments {

	private float amount;
	  Currency CurrencyObject;
	  PaymentMethod PaymentMethodObject;
	  private String transactionTime;


	 // Getter Methods 

	  public float getAmount() {
	    return amount;
	  }

	  public Currency getCurrency() {
	    return CurrencyObject;
	  }

	  public PaymentMethod getPaymentMethod() {
	    return PaymentMethodObject;
	  }

	  public String getTransactionTime() {
	    return transactionTime;
	  }

	 // Setter Methods 

	  public void setAmount( float amount ) {
	    this.amount = amount;
	  }

	  public void setCurrency( Currency currencyObject ) {
	    this.CurrencyObject = currencyObject;
	  }

	  public void setPaymentMethod( PaymentMethod paymentMethodObject ) {
	    this.PaymentMethodObject = paymentMethodObject;
	  }

	  public void setTransactionTime( String transactionTime ) {
	    this.transactionTime = transactionTime;
	  }
	}
	
