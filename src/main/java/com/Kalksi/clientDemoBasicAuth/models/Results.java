package com.Kalksi.clientDemoBasicAuth.models;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class Results {

	private boolean active;
	  private String id;
	  private String number;
	  private float revision;
	  private String bookingTime;
	  private boolean cancelled;
	  Cashier CashierObject;
	  private String creationTime;
	  Currency CurrencyObject;
	  CustomerGroup CustomerGroupObject;
	  private List<Items> items = new ArrayList<Items>();
	  OrganizationalUnit OrganizationalUnitObject;
	  ArrayList<Payments> payments = new ArrayList<Payments>();
	  PointOfSale PointOfSaleObject;
	  PriceGroup PriceGroupObject;
	  private boolean voided;
	  Warehouse WarehouseObject;
	  private float zcounter;


	 // Getter Methods 

	  public Cashier getCashierObject() {
		return CashierObject;
	}

	public void setCashierObject(Cashier cashierObject) {
		CashierObject = cashierObject;
	}

	public Currency getCurrencyObject() {
		return CurrencyObject;
	}

	public void setCurrencyObject(Currency currencyObject) {
		CurrencyObject = currencyObject;
	}

	public CustomerGroup getCustomerGroupObject() {
		return CustomerGroupObject;
	}

	public void setCustomerGroupObject(CustomerGroup customerGroupObject) {
		CustomerGroupObject = customerGroupObject;
	}

	public List<Items> getItems() {
		return items;
	}

	public void setItems(List<Items> items) {
		this.items = items;
	}

	public OrganizationalUnit getOrganizationalUnitObject() {
		return OrganizationalUnitObject;
	}

	public void setOrganizationalUnitObject(OrganizationalUnit organizationalUnitObject) {
		OrganizationalUnitObject = organizationalUnitObject;
	}

	public ArrayList<Payments> getPayments() {
		return payments;
	}

	public void setPayments(ArrayList<Payments> payments) {
		this.payments = payments;
	}

	public PointOfSale getPointOfSaleObject() {
		return PointOfSaleObject;
	}

	public void setPointOfSaleObject(PointOfSale pointOfSaleObject) {
		PointOfSaleObject = pointOfSaleObject;
	}

	public PriceGroup getPriceGroupObject() {
		return PriceGroupObject;
	}

	public void setPriceGroupObject(PriceGroup priceGroupObject) {
		PriceGroupObject = priceGroupObject;
	}

	public Warehouse getWarehouseObject() {
		return WarehouseObject;
	}

	public void setWarehouseObject(Warehouse warehouseObject) {
		WarehouseObject = warehouseObject;
	}

	public boolean getActive() {
	    return active;
	  }

	  public String getId() {
	    return id;
	  }

	  public String getNumber() {
	    return number;
	  }

	  public float getRevision() {
	    return revision;
	  }

	  public String getBookingTime() {
	    return bookingTime;
	  }

	  public boolean getCancelled() {
	    return cancelled;
	  }

	  public Cashier getCashier() {
	    return CashierObject;
	  }

	  public String getCreationTime() {
	    return creationTime;
	  }

	  public Currency getCurrency() {
	    return CurrencyObject;
	  }

	  public CustomerGroup getCustomerGroup() {
	    return CustomerGroupObject;
	  }

	  public OrganizationalUnit getOrganizationalUnit() {
	    return OrganizationalUnitObject;
	  }

	  public PointOfSale getPointOfSale() {
	    return PointOfSaleObject;
	  }

	  public PriceGroup getPriceGroup() {
	    return PriceGroupObject;
	  }

	  public boolean getVoided() {
	    return voided;
	  }

	  public Warehouse getWarehouse() {
	    return WarehouseObject;
	  }

	  public float getZcounter() {
	    return zcounter;
	  }

	 // Setter Methods 

	  public void setActive( boolean active ) {
	    this.active = active;
	  }

	  public void setId( String id ) {
	    this.id = id;
	  }

	  public void setNumber( String number ) {
	    this.number = number;
	  }

	  public void setRevision( float revision ) {
	    this.revision = revision;
	  }

	  public void setBookingTime( String bookingTime ) {
	    this.bookingTime = bookingTime;
	  }

	  public void setCancelled( boolean cancelled ) {
	    this.cancelled = cancelled;
	  }

	  public void setCashier( Cashier cashierObject ) {
	    this.CashierObject = cashierObject;
	  }

	  public void setCreationTime( String creationTime ) {
	    this.creationTime = creationTime;
	  }

	  public void setCurrency( Currency currencyObject ) {
	    this.CurrencyObject = currencyObject;
	  }

	  public void setCustomerGroup( CustomerGroup customerGroupObject ) {
	    this.CustomerGroupObject = customerGroupObject;
	  }

	  public void setOrganizationalUnit( OrganizationalUnit organizationalUnitObject ) {
	    this.OrganizationalUnitObject = organizationalUnitObject;
	  }

	  public void setPointOfSale( PointOfSale pointOfSaleObject ) {
	    this.PointOfSaleObject = pointOfSaleObject;
	  }

	  public void setPriceGroup( PriceGroup priceGroupObject ) {
	    this.PriceGroupObject = priceGroupObject;
	  }

	  public void setVoided( boolean voided ) {
	    this.voided = voided;
	  }

	  public void setWarehouse( Warehouse warehouseObject ) {
	    this.WarehouseObject = warehouseObject;
	  }

	  public void setZcounter( float zcounter ) {
	    this.zcounter = zcounter;
	  }

	@Override
	public String toString() {
		return "Results [active=" + active + ", id=" + id + ", number=" + number + ", revision=" + revision
				+ ", bookingTime=" + bookingTime + ", cancelled=" + cancelled + ", CashierObject=" + CashierObject
				+ ", creationTime=" + creationTime + ", CurrencyObject=" + CurrencyObject + ", CustomerGroupObject="
				+ CustomerGroupObject + ", items=" + items + ", OrganizationalUnitObject=" + OrganizationalUnitObject
				+ ", payments=" + payments + ", PointOfSaleObject=" + PointOfSaleObject + ", PriceGroupObject="
				+ PriceGroupObject + ", voided=" + voided + ", WarehouseObject=" + WarehouseObject + ", zcounter="
				+ zcounter + "]";
	}
	  
	  
	  
	}
