package com.Kalksi.clientDemoBasicAuth.models;

public class Currency {
	  private String id;
	  private String name;
	  private String number;


	 // Getter Methods 

	  public String getId() {
	    return id;
	  }

	  public String getName() {
	    return name;
	  }

	  public String getNumber() {
	    return number;
	  }

	 // Setter Methods 

	  public void setId( String id ) {
	    this.id = id;
	  }

	  public void setName( String name ) {
	    this.name = name;
	  }

	  public void setNumber( String number ) {
	    this.number = number;
	  }

	@Override
	public String toString() {
		return "Currency [id=" + id + ", name=" + name + ", number=" + number + "]";
	}
	  
	  
	  
	}
