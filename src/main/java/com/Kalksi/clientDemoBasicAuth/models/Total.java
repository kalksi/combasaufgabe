package com.Kalksi.clientDemoBasicAuth.models;

import java.util.ArrayList;
import java.util.List;

public class Total {
	  private float base;
	  private float discount;
	  private float gross;
	  private float net;
	  List <TaxPayments> taxPayments = new ArrayList<TaxPayments>();


	 // Getter Methods 

	  public List<TaxPayments> getTaxPayments() {
		return taxPayments;
	}

	public void setTaxPayments(List<TaxPayments> taxPayments) {
		this.taxPayments = taxPayments;
	}

	public float getBase() {
	    return base;
	  }

	  public float getDiscount() {
	    return discount;
	  }

	  public float getGross() {
	    return gross;
	  }

	  public float getNet() {
	    return net;
	  }

	 // Setter Methods 

	  public void setBase( float base ) {
	    this.base = base;
	  }

	  public void setDiscount( float discount ) {
	    this.discount = discount;
	  }

	  public void setGross( float gross ) {
	    this.gross = gross;
	  }

	  public void setNet( float net ) {
	    this.net = net;
	  }

	@Override
	public String toString() {
		return "Total [base=" + base + ", discount=" + discount + ", gross=" + gross + ", net=" + net + ", taxPayments="
				+ taxPayments + "]";
	}
	  
	  
	  
	}