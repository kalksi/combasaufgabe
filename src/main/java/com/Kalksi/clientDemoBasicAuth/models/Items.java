package com.Kalksi.clientDemoBasicAuth.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Items {
	private String bookingTime;
	private CommodityGroup CommodityGroupObject;
	private Integer indent;
	private boolean manualPrice;
	private Product ProductObject;
	private float quantity;
	private String recognitionNumber;
	private Sector SectorObject;
	private Total TotalObject;

	// Getter Methods

	public CommodityGroup getCommodityGroupObject() {
		return CommodityGroupObject;
	}

	public void setCommodityGroupObject(CommodityGroup commodityGroupObject) {
		CommodityGroupObject = commodityGroupObject;
	}

	public Product getProductObject() {
		return ProductObject;
	}

	public void setProductObject(Product productObject) {
		ProductObject = productObject;
	}

	public Sector getSectorObject() {
		return SectorObject;
	}

	public void setSectorObject(Sector sectorObject) {
		SectorObject = sectorObject;
	}

	public Total getTotalObject() {
		return TotalObject;
	}

	public void setTotalObject(Total totalObject) {
		TotalObject = totalObject;
	}

	public String getBookingTime() {
		return bookingTime;
	}

	public CommodityGroup getCommodityGroup() {
		return CommodityGroupObject;
	}

	public Integer getIndent() {
		return indent;
	}

	public boolean getManualPrice() {
		return manualPrice;
	}

	public Product getProduct() {
		return ProductObject;
	}

	public float getQuantity() {
		return quantity;
	}

	public String getRecognitionNumber() {
		return recognitionNumber;
	}

	public Sector getSector() {
		return SectorObject;
	}

	public Total getTotal() {
		return TotalObject;
	}

	// Setter Methods

	public void setBookingTime(String bookingTime) {
		this.bookingTime = bookingTime;
	}

	public void setCommodityGroup(CommodityGroup commodityGroupObject) {
		this.CommodityGroupObject = commodityGroupObject;
	}

	public void setIndent(Integer indent) {
		this.indent = indent;
	}

	public void setManualPrice(boolean manualPrice) {
		this.manualPrice = manualPrice;
	}

	public void setProduct(Product productObject) {
		this.ProductObject = productObject;
	}

	public void setQuantity(float quantity) {
		this.quantity = quantity;
	}

	public void setRecognitionNumber(String recognitionNumber) {
		this.recognitionNumber = recognitionNumber;
	}

	public void setSector(Sector sectorObject) {
		this.SectorObject = sectorObject;
	}

	public void setTotal(Total totalObject) {
		this.TotalObject = totalObject;
	}

	@Override
	public String toString() {
		return "Items [bookingTime=" + bookingTime + ", CommodityGroupObject=" + CommodityGroupObject + ", indent="
				+ indent + ", manualPrice=" + manualPrice + ", ProductObject=" + ProductObject + ", quantity="
				+ quantity + ", recognitionNumber=" + recognitionNumber + ", SectorObject=" + SectorObject
				+ ", TotalObject=" + TotalObject + "]";
	}

}
