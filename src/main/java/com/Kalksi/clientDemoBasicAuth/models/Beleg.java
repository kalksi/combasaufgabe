package com.Kalksi.clientDemoBasicAuth.models;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class Beleg {
	 private Integer currentPage;
	 @JsonIgnoreProperties(ignoreUnknown = true)
	 private Links links;
	  public Links getLinks() {
		return links;
	}
	public void setLinks(Links links) {
		this.links = links;
	}
	private Integer pagesTotal;
	  ArrayList<Results> results = new ArrayList<Results>();
	  private Integer resultsOfPage;
	  private Integer resultsTotal;
	public Integer getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(Integer currentPage) {
		this.currentPage = currentPage;
	}
	public Links getLinksObject() {
		return links;
	}
	public void setLinksObject(Links linksObject) {
		links = linksObject;
	}
	public Integer getPagesTotal() {
		return pagesTotal;
	}
	public void setPagesTotal(Integer pagesTotal) {
		this.pagesTotal = pagesTotal;
	}
	public ArrayList<Results> getResults() {
		return results;
	}
	public void setResults(ArrayList<Results> results) {
		this.results = results;
	}
	public Integer getResultsOfPage() {
		return resultsOfPage;
	}
	public void setResultsOfPage(Integer resultsOfPage) {
		this.resultsOfPage = resultsOfPage;
	}
	public Integer getResultsTotal() {
		return resultsTotal;
	}
	public void setResultsTotal(Integer resultsTotal) {
		this.resultsTotal = resultsTotal;
	}
	@Override
	public String toString() {
		return "Beleg [currentPage=" + currentPage + ", links=" + links + ", pagesTotal=" + pagesTotal + ", results="
				+ results + ", resultsOfPage=" + resultsOfPage + ", resultsTotal=" + resultsTotal + "]";
	}


	
}
