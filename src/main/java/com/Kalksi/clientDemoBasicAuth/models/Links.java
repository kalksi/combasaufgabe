package com.Kalksi.clientDemoBasicAuth.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Links {
	  private String self;


	 // Getter Methods 

	  public String getSelf() {
	    return self;
	  }

	 // Setter Methods 

	  public void setSelf( String self ) {
	    this.self = self;
	  }

	@Override
	public String toString() {
		return "Links [self=" + self + "]";
	}
	  
	  
}