package com.Kalksi.clientDemoBasicAuth.models;

public class TaxPayments {

	private float amount;
	  private float taxRate;
	  private boolean included;
	  SalesTax SalesTaxObject;


	 // Getter Methods 

	  public float getAmount() {
	    return amount;
	  }

	  public float getTaxRate() {
	    return taxRate;
	  }

	  public boolean getIncluded() {
	    return included;
	  }

	  public SalesTax getSalesTax() {
	    return SalesTaxObject;
	  }

	 // Setter Methods 

	  public void setAmount( float amount ) {
	    this.amount = amount;
	  }

	  public void setTaxRate( float taxRate ) {
	    this.taxRate = taxRate;
	  }

	  public void setIncluded( boolean included ) {
	    this.included = included;
	  }

	  public void setSalesTax( SalesTax salesTaxObject ) {
	    this.SalesTaxObject = salesTaxObject;
	  }

	@Override
	public String toString() {
		return "TaxPayments [amount=" + amount + ", taxRate=" + taxRate + ", included=" + included + ", SalesTaxObject="
				+ SalesTaxObject + "]";
	}
	  
	  
	  
	}
	