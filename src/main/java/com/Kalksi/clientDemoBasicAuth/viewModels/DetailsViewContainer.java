package com.Kalksi.clientDemoBasicAuth.viewModels;

import java.util.List;

public class DetailsViewContainer extends SimpleViewContainer{
	
	private String kassiere;
	private List<Artikelpositionen> artikelpositionen;
	private List<Zahlungsmittel> zahlungsmittel;

	public DetailsViewContainer(String belegnummer, String kasse, String organisation, String verkaufszeit,
			Float gesamtbetrag) {
		super(belegnummer, kasse, organisation, verkaufszeit, gesamtbetrag);
		// TODO Auto-generated constructor stub
	}

	public String getKassiere() {
		return kassiere;
	}

	public void setKassiere(String kassiere) {
		this.kassiere = kassiere;
	}

	public List<Artikelpositionen> getArtikelpositionen() {
		return artikelpositionen;
	}

	public void setArtikelpositionen(List<Artikelpositionen> artikelpositionen) {
		this.artikelpositionen = artikelpositionen;
	}

	public List<Zahlungsmittel> getZahlungsmittel() {
		return zahlungsmittel;
	}

	public void setZahlungsmittel(List<Zahlungsmittel> zahlungsmittel) {
		this.zahlungsmittel = zahlungsmittel;
	}

	public DetailsViewContainer(String belegnummer, String kasse, String organisation, String verkaufszeit,
			Float gesamtbetrag, String kassiere, List<Artikelpositionen> artikelpositionen,
			List<Zahlungsmittel> zahlungsmittel) {
		super(belegnummer, kasse, organisation, verkaufszeit, gesamtbetrag);
		this.kassiere = kassiere;
		this.artikelpositionen = artikelpositionen;
		this.zahlungsmittel = zahlungsmittel;
	}
	public DetailsViewContainer(SimpleViewContainer obj,
		   String kassiere, List<Artikelpositionen> artikelpositionen,
			List<Zahlungsmittel> zahlungsmittel) {
		super(obj.getBelegnummer(), obj.getKasse(), obj.getOrganisation(), obj.getVerkaufszeit(), obj.getGesamtbetrag());
		this.kassiere = kassiere;
		this.artikelpositionen = artikelpositionen;
		this.zahlungsmittel = zahlungsmittel;
	}

	@Override
	public String toString() {
		return "DetailsViewContainer [kassiere=" + kassiere + ", artikelpositionen=" + artikelpositionen
				+ ", zahlungsmittel=" + zahlungsmittel + "]";
	}


	
	
}
