package com.Kalksi.clientDemoBasicAuth.viewModels;

public class SimpleViewContainer {
	
	private String Belegnummer;
	private String Kasse;
	private String Organisation;
	private String Verkaufszeit;
	private Float Gesamtbetrag;
	public SimpleViewContainer(String belegnummer, String kasse, String organisation, String verkaufszeit,
			Float gesamtbetrag) {
		super();
		Belegnummer = belegnummer;
		Kasse = kasse;
		Organisation = organisation;
		Verkaufszeit = verkaufszeit;
		Gesamtbetrag = gesamtbetrag;
	}
	public String getBelegnummer() {
		return Belegnummer;
	}
	public void setBelegnummer(String belegnummer) {
		Belegnummer = belegnummer;
	}
	public String getKasse() {
		return Kasse;
	}
	public void setKasse(String kasse) {
		Kasse = kasse;
	}
	public String getOrganisation() {
		return Organisation;
	}
	public void setOrganisation(String organisation) {
		Organisation = organisation;
	}
	public String getVerkaufszeit() {
		return Verkaufszeit;
	}
	public void setVerkaufszeit(String verkaufszeit) {
		Verkaufszeit = verkaufszeit;
	}
	public Float getGesamtbetrag() {
		return Gesamtbetrag;
	}
	public void setGesamtbetrag(Float gesamtbetrag) {
		Gesamtbetrag = gesamtbetrag;
	}
	@Override
	public String toString() {
		return "[Belegnummer=" + Belegnummer + ", Kasse=" + Kasse + ", Organisation=" + Organisation
				+ ", Verkaufszeit=" + Verkaufszeit + ", Gesamtbetrag=" + Gesamtbetrag + "]";
	}
	

}
