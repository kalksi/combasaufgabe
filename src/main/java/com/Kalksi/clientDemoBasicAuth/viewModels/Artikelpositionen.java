package com.Kalksi.clientDemoBasicAuth.viewModels;

public class Artikelpositionen {

	private String name;
	private float Menge;
	private float Price;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public float getMenge() {
		return Menge;
	}
	public void setMenge(float menge) {
		Menge = menge;
	}
	public float getPrice() {
		return Price;
	}
	public void setPrice(float price) {
		Price = price;
	}
	public Artikelpositionen(String name, float f, float price) {
		super();
		this.name = name;
		Menge = f;
		Price = price;
	}
	@Override
	public String toString() {
		return "Artikelpositionen [name=" + name + ", Menge=" + Menge + ", Price=" + Price + "]";
	}
	
}
