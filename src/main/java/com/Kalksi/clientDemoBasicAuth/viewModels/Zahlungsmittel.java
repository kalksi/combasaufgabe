package com.Kalksi.clientDemoBasicAuth.viewModels;

public class Zahlungsmittel {
	
	private String name;
	private float betrag;
	public Zahlungsmittel(String name, float betrag) {
		super();
		this.name = name;
		this.betrag = betrag;
	}
	public float getBetrag() {
		return betrag;
	}
	public void setBetrag(float betrag) {
		this.betrag = betrag;
	}
	@Override
	public String toString() {
		return "Zahlungsmittel [name=" + name + ", betrag=" + betrag + "]";
	}

}
