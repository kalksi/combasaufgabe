package com.Kalksi.clientDemoBasicAuth.services;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.Base64;

import com.Kalksi.clientDemoBasicAuth.models.Beleg;
import com.Kalksi.clientDemoBasicAuth.models.Results;
import com.fasterxml.jackson.databind.ObjectMapper;

public class BelegeService implements Interface_BelegeService {

	private HttpClient client;
	private String encoding;

	public BelegeService() {
		client = HttpClient.newHttpClient();
		encoding = Base64.getEncoder().encodeToString(("kha@combase.de:(13topsecret37]").getBytes());
	}

	@Override
	public Results getByID(String URL, String id) {
		HttpRequest request = HttpRequest.newBuilder().uri(URI.create(URL+"/"+id))
				.setHeader("Authorization", "Basic " + encoding).build();
		
		
		try {
			HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
			return new ObjectMapper().readValue(response.body(), Results.class);
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		

		return null;
	}

	@Override
	public Beleg getAll(String URL) {
		

		HttpRequest request = HttpRequest.newBuilder().uri(URI.create(URL))
				.setHeader("Authorization", "Basic " + encoding).build();
		//System.out.println(request.headers());
		

		try {
			HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
			return new ObjectMapper().readValue(response.body(), Beleg.class);
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public Beleg getAllByPointOfSale(String URL, String PointID) {
		
		
		HttpRequest request = HttpRequest.newBuilder().uri(URI.create(URL+PointID+"/receipts"))
				.setHeader("Authorization", "Basic " + encoding).build();
		//System.out.println(request.headers());
		System.out.println(request);

		try {
			HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
			return new ObjectMapper().readValue(response.body(), Beleg.class);
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		
		return null;
	}

	@Override
	public Results getOneByPointOfSale(String URL, String PointID, String id) {
		
		

		HttpRequest request = HttpRequest.newBuilder().uri(URI.create(URL+PointID+"/receipts/"+id))
				.setHeader("Authorization", "Basic " + encoding).build();
		System.out.println(request);
		

		try {
			HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
			return new ObjectMapper().readValue(response.body(), Results.class);
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		return null;
	}

}
