package com.Kalksi.clientDemoBasicAuth.services;

import com.Kalksi.clientDemoBasicAuth.models.Beleg;
import com.Kalksi.clientDemoBasicAuth.models.Results;

public interface Interface_BelegeService {
	
	public Results getByID(String URL,String id);
	
	public Beleg getAll(String URL);
	
	public Beleg getAllByPointOfSale(String URL,String PointID);
	
	public Results getOneByPointOfSale(String URL,String PointID,String id);
	
	

}
